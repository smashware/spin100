.. include:: text_roles.txt

.. _cd_pipelines:

*******************
CI/CD  Pipelines
*******************
In this lab, we'll cover deployment using pipelines. We'll create a  CI/CD pipeline for a small 
application that will leverrage red/black deployment strategies, integrate with CI tools, and deploy 
to  Staging and Production clusters.

.. sectnum::
    :suffix: .
    :start: 4

+----------------------------+----------------------------------------------------------------------+
| **Chapter Details**                                                                               |
+============================+======================================================================+
| **Chapter Goal**           | Set up Github. Create triggers for Docker Hub and Github. Create     |
|                            | pipelines and execute them.                                          |
+----------------------------+----------------------------------------------------------------------+
|                            | | :ref:`create_ns`                                                   | 
|                            | | :ref:`spinnaker_app`                                               |
|                            | | :ref:`staging_lb`                                                  |
|                            | | :ref:`production_lb`                                               |
|                            | | :ref:`deploy_staging`                                              |
|                            | | :ref:`deploy_production`                                           |
|                            | | :ref:`verify_initial_state`                                        |
|                            | | :ref:`create_staging_pipeline`                                     |
|                            | | :ref:`create_verify_pipeline`                                      |              
|                            | | :ref:`create_production_pipeline`                                  |
|                            | | :ref:`site_change`                                                 |
+----------------------------+----------------------------------------------------------------------+

.. raw:: pdf

    PageBreak

.. _create_ns:

Create kubernetes namespace for site
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
All pods and services for the site application will be created in this namespace

**Step 1** Create a namespace for the site application::

    $ kubectl create ns site
    namespace "site" created

.. _spinnaker_app:

Create site  Application in Spinnaker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Step 1** Get the spinnaker dashboard url, in the following, paste it in place ``spinnaker_dashboard``::

  $ echo http://`curl -s ifconfig.co`:`kubectl -n spinnaker get svc expose-deck -o jsonpath='{.spec.ports[0].nodePort}'`

**Step 2** Open in browser http://``spinnaker_dashboard``

**Step 3** Click ``Applications``

**Step 4** Click ``Actions`` -> ``Create application``

**Step 5** Fill ``New Application`` form

 * Name: site
 * Owner Email: your email
 * All other options should be default.

**Step 6** Click ``Create`` button

.. _staging_lb:

Create Staging load balancer (Browser)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Our k8s cluster doesn't support any cloudproviders because we installed in single-node mode.
In this case, we can use only NodePort as the load balancer to expose our application service.

**Step 1** Click ``Applications``

**Step 2** Click on ``site`` application

**Step 3** Click on ``LOAD BALANCERS`` -> ``Create Load Balancer``

**Step 4** Fill ``Create New Load balancer``

* Account: local
* Namespace: site
* Stack: staging
* Advanced Settings->Type: NodePort
* Node Port: 30010
* All other options should be default.
* Click ``Create`` button


.. _production_lb:

Create Production load balancer (Browser)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Step 1** Click ``Applications``

**Step 2** Click on ``site`` application

**Step 3** Click on ``LOAD BALANCERS`` -> ``Create Load Balancer``

**Step 4** Fill ``Create New Load balancer``

* Account: local
* Namespace: site
* Stack: production
* Advanced Settings->Type: NodePort
* Node Port: 30020
* All other options should be default.
* Click ``Create`` button

.. _deploy_staging:

Deploy Staging Server Group (Browser)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Step 1** Click ``CLUSTERS`` -> Push ``Create Server Group``

**Step 2** Fill in the following under ``Basic Settings``

* Account: local
* Namespace: site
* Stack: staging
* Containers: index.docker.io/your_dockerhub_account/site:v1.0

**Step 3** Load Balancers -> Select ``site-staging`` -> Push button ``Create``

* All other options should be default.
* Push ``Create`` button
* Result:
  
  * .. figure:: /screenshots/spin-stagecluster.png

.. _deploy_production:

Deploy Production Server Group (Browser)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Step 1** Click ``CLUSTERS`` -> Push ``Create Server Group``

**Step 2** Fill in the following under ``Basic Settings``

* Account: local
* Namespace: site
* Stack: production
* Containers: index.docker.io/your_dockerhub_account/site:v1.0

**Step 3** Load Balancers -> Select ``site-production``

**Step 4** Replicas -> Set ``Capactiy`` to 3

* All other options should be default.
* Push ``Create`` button
* Result:
  
  * .. figure:: /screenshots/spin-prodcluster.png

.. _verify_initial_state:

Verify Staging and Production initial State (Browser)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Step 1** Open Staging and Production application in browser

* http://<lab ip>:30010
* Result:
  
  * .. figure:: /screenshots/spin-stagingv1.png

* http://<lab ip>:30020
* Result:
  
  * .. figure:: /screenshots/spin-prodv1.png

**Step 2** Verify the backend pods existing kubernetes cluster::
 
  $ kubectl get pods -n site
  NAME                         READY     STATUS    RESTARTS   AGE
  site-production-v000-2p7dx   1/1       Running   0          11m
  site-production-v000-725z6   1/1       Running   0          11m
  site-production-v000-gcl8t   1/1       Running   0          11m
  site-staging-v000-dzfbw      1/1       Running   0          17m

.. _create_staging_pipeline:

Deploy Staging Pipeline
~~~~~~~~~~~~~~~~~~~~~~~~~

**Step 1** Click ``pipelines`` -> ``Create a new pipeline``

**Step 2** Fill the form ``Create New Pipeline``

* Type: Pipeline
* Pipeline Name: Deploy to Staging
* Push ``Create`` button

**Step 3** Create Automated trigger from Docker

* Configuration
  
  * Automated Triggers
  * Add Trigger
  
    * Type: Docker Registry
    * Registry Name: dockerhub
    * Organization: <dockerid>
    * Image: <dockerid>/site
    * Tag: Leave blank

**Step 4** Add Clean up  Stage

* Click add stage
* Type: Destroy Server Group
* Namespaces: site
* Cluster: site-staging
* All other options should be default

**Step 5** Add Deploy stage

* Click add stage
* Type: Deploy
* Click: Add server group
  
  * Copy configuration from: None
  * Click ``Continue without a template``
  
    * Account: Local
    * Namespace: site
    * Stack; Staging
    * Container: index.docker.io/<dockerid>/site (Tag resolved at runtin)
    * Strategy: None
    * Load Balancer: site-staging
    * All other options should be default

**Step 6** Add automate QA stage

* Click add stage
* Type: Jenkins
* Stage Name: QA
* Master: default
* Job: QA site
* SITE_URL: site-staging.site.svc.cluster.local

.. _create_verify_pipeline:

Verify Application
~~~~~~~~~~~~~~~~~~

**Step 1** Click ``pipelines`` -> ``Create a new pipeline``

**Step 2** Fill the form ``Create New Pipeline``

* Type: Pipeline
* Pipeline Name: Verify Application
* Push ``Create`` button

**Step 3** Create Automated trigger from Deploy to Staging pipeline

* Configuration
  
  * Automated Triggers
  * Add Trigger
  
    * Type: Pipeline
    * Application: site
    * Pipeline: Deploy to Staging
    * Pipeline Status: successful
    * Trigger Enabled: Checked


**Step 4** Add Manual Judgement

* Type: Manual Judgment
* Instructions: Verify that staging looks okay: http://<lab ip>:30010
* All other options should be default


.. _create_production_pipeline:

Create Production Pipeline
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Step 1** Click ``pipelines`` -> ``Create a new pipeline``

**Step 2** Fill the form ``Create New Pipeline``

* Type: Pipeline
* Pipeline Name: Promote to Production
* Push ``Create`` button

**Step 3** Create Automated trigger from ``Verify Application`` pipeline

* Configuration
  
  * Automated Triggers
  * Add Trigger
  
    * Type: Pipeline
    * Application: site
    * Pipeline: Verify Application
    * Pipeline Status: successful
    * Trigger Enabled: Checked
* Click ``add stage``
* Type: Destroy Server Group
* Namespaces: site
* Cluster: production
* All other options should be default

**Step 3** Find verified image from Staging to Deploy to Production

* Click ``add stage``
* Type: Find Image from Cluster
* Account: local
* Namespaces: site
* Cluster: site-staging
* Server Group Selection: Newest
* leave rest as default

**Step 4** Deploy to Productuion with Red/Black strategy

* Click ``add stage``
* Type: Deploy
* Click: Add server group
* Copy configuration from:None
* Click 'Continue without a template'
  
  * Account: local
  * Namespace: site
  * Stack: production
  * Containers: site-staging undefined
  * Strategy: Red/Black
  * Load Balancers: site-production  
  * All other options should be default

**Step 5** add QA stage

* Click ``add stage``
* Type: Jenkins
* Stage Name: QA
* Master: default
* Job: QA site
* SITE_URL: site-production.site.svc.cluster.local
* All other options should be default.

**Step 6** Add Manual Judgement

* Type: Manual Judgement
* Instructions: Verify that production looks okay: http://<lab ip>:30020
* All other options should be default

**Step 7** Click ``Save Changes``

.. _site_change:

Update application and trigger new deployment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To test our CI/CD work flow. Let's pretend we're developers and update code in our tiny web application.

**Step 1** Edit `~site/site.html` and change color from green to red::

  $ cd ~/site
  $ vim site.html 
  change background color to red 
  $ git commit -m 'change color' site.html
  $ git tag v1.1
  $ git push --tags
  Username: <githubid>
  Password: <githubpassword>

**Step 2** Verify staging progress
 
  * Check Pipeline page in UI
    
    * http://lab_ip:31000
    * .. figure:: /screenshots/spin-staging-progress.png
  * Check Staging Application page has new color
    
    * http://lab_ip:30010
    * .. figure:: /screenshots/spin-red.png
  * Check the Automated QA step 
   
   * Http://lab_ip:31001 -> QA site
   * .. figure:: /screenshots/spin-jenkins-qa.png

**Step 2** Complete Manual Judgement for Verify Pipeline

  * When Staging looks good click continue
  * .. figure:: /screenshots/spin-manual.png

**Step 3** Verify Production and complete manual judgement.

  * Check Pipeline page in UI
    
    * http://lab_ip:31000
    * .. figure:: /screenshots/spin-production.png
  * Check Production Application page has new color
    
    * http://lab_ip:30020
    * .. figure:: /screenshots/spin-red.png
 
 * Check the Automated QA step 

   * Http://lab_ip:31001 -> QA site
   * .. figure:: /screenshots/spin-jenkins-qa.png

 * Confirm Production site with manual judgement affirmation.

.. admonition:: Checkpoint
    :class: conventions checkpoints

    * Create Clusters
    * Create Load balancers
    * Set up pipelines
    * Link pipelines
    * Connect pipelines to Docker trigger
    * Create red/black stages
    * Continuous delivery: code change -> Production
