.. include:: text_roles.txt

.. _lab_env:

*********************
Classroom Environment
*********************

.. sectnum::
    :suffix: .
    :start: 1

Before diving into Docker and Kubernetes, let's take some time to explore the
classroom environment. Understanding how things are currently configured is
beneficial for your learning as you move forward in the class.

In this lab, we will explore this currently configured environment to help with
the execution of future labs.

+----------------------------+----------------------------------------------------------------------+
| **Chapter Details**                                                                               |
+============================+======================================================================+
| **Chapter Goal**           | Explore the classroom environment                                    |
+----------------------------+----------------------------------------------------------------------+
| **Chapter Sections**       | | :ref:`env_notes`                                                   |
|                            | | :ref:`env_access`                                                  |
|                            | | :ref:`env_troubleshoot`                                            |
+----------------------------+----------------------------------------------------------------------+

.. raw:: pdf

    PageBreak

.. _env_notes:

Organizational Notes
--------------------

All students have their own environments to perform exercises independently.


.. _env_access:

Working with Remote Environment
-------------------------------

For command line steps you need an SSH client:

* For Linux and OS X you should already have it.
* For Windows you can download the PuTTY SSH client from the Internet.

When working with files from command line, you can use any editors or file
readers that you are familiar with and are available in default installation.
Here are examples:

* Edit - ``vi``, ``vim``, or ``nano``
* View - ``less``, ``more``, ``cat``, or ``tail``

Refer to the ``man`` page for specific command for details on options and
usage.

**Step 1** From your laptop’s shell environment or using PuTTY, connect via SSH
protocol to the remote environment provided by your instructor. If you did not
receive information on your personal lab environment, please ask your
instructor. Log in using ``smashware`` as the user name and ``dev0p$101`` as the
password::

    ssh smashware@<lab IP>

**Step 2** Check that you can execute commands as ``root`` user::

    smashware@lab:~$ sudo su
    root@lab:/home/smashware#

**Step 3** Collect the system configuration::

    root@lab:/home/smashware# lscpu | head -n5
    Architecture:          x86_64
    CPU op-mode(s):        32-bit, 64-bit
    Byte Order:            Little Endian
    CPU(s):                4
    On-line CPU(s) list:   0-3


    root@lab:/home/smashware# cat /proc/meminfo | head -n3
    MemTotal:       16431412 kB
    MemFree:         1939528 kB
    MemAvailable:    8458444 kB

    root@lab:/home/smashware# lsblk
    NAME    MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
    xvda    202:0    0  32G  0 disk
    `-xvda1 202:1    0  32G  0 part /


**Step 3** To close the ``root`` session press ``Ctrl-D`` or execute ``exit``::

    root@lab:/home/smashware# exit
    smashware@lab:~$

.. _env_troubleshoot:

Frequently Asked Questions
--------------------------

This list contains  frequently asked questions that pertain to the environment
or exercises.

* The specific step failed, or I cannot initiate the next step for some reason

  The environment allows you to be flexible in choosing names, IP addresses and
  other parameters for the steps in this course. However, the chapters in the
  course are connected to each other in such a way that the results from one
  chapter are required for the next one. We highly recommend that you use
  names, IP addresses and other parameters exactly as they are specified in the
  steps below. Also, using the same parameters will help us identify the issue.
  We highly recommend that you execute all the steps as listed. Please do not
  skip any steps or make any additional unspecified steps, such as changing the
  default password.

  If one specific step failed (for example, you have received unexpected error
  message), or you cannot initiate the next step for some reason, please check
  that you executed all previous steps exactly as they are specified.

  If you feel as you executed all the steps correctly, ask the instructor. Be
  sure to let them know of any variations you may have made to the standard
  flow. The instructor will help you solve the issue, and in the worst case,
  can revert the whole environment to the one of the predefined state, so you
  can continue working with the class.

.. admonition:: Checkpoint
    :class: conventions checkpoints

    * Receive a lab environment assignment
    * Use SSH client to access the environment
    * Read Frequently Asked Questions
