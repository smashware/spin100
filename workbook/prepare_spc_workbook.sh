#!/usr/bin/env bash

echo "Preparing SPC version of workbook"
for file in _styles/layout.html conf.py index.rst environment.rst
do
    bash ./generate_spc_version.sh $file
done