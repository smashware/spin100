.. include:: text_roles.txt

.. _application_management:

**********************
Application Management
**********************
In this lab, we'll cover cluster operations in Spinnaker. We'll take you through the operations in 
deck like: rollback, replica sets, traffic guards, and review task logs. 

.. sectnum::
    :suffix: .
    :start: 5

+----------------------------+----------------------------------------------------------------------+
| **Chapter Details**                                                                               |
+============================+======================================================================+
| **Chapter Goal**           | Manage deployed application, set up safe guards, roll back, increase |
|                            | replicas                                                             |
+----------------------------+----------------------------------------------------------------------+
|                            | | :ref:`rollback`                                                    |
|                            | | :ref:`verify_rollback`                                             |
|                            | | :ref:`scalling`                                                    |
|                            | | :ref:`terminate`                                                   |
|                            | | :ref:`availability`                                                |
|                            | | :ref:`restore`                                                     |
|                            | | :ref:`destroy`                                                     |
+----------------------------+----------------------------------------------------------------------+

.. raw:: pdf

    PageBreak

.. _rollback:

Rollback Application
~~~~~~~~~~~~~~~~~~~~~
Rollback to previous version of the applcation

**Step 1** Connect to Spinnaker cluster page

    * Point your browser to <labIP>:31000
    * Click ``Applications``
    * Click ``site``
    * Click ``Clusters``

**Step 2** Bring up Server Groups actions

    * Under site-production click on the latest enabled server group
      
      *  .. figure:: /screenshots/spin-cluster.png
    * The server group details will show up on the right side
      
      *  .. figure:: /screenshots/spin-servergroupactions.png
    * Click ``Server Group Actions``


**Step 3** Roll back

    * Select Rollback
    * Restore to <previous Server Group> and fill in reason as seen in picture below.
      
      *  .. figure:: /screenshots/spin-rollbackscreen.png
    * Type Local and click ``submit``

.. _verify_rollback:

Verify Rollback of Apllication
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Check that the application has been restored to the previous version

**Step 1** Check the cluster screen

   * Verify that latest server group is disabled and prvious is enabled
     
      *  .. figure:: /screenshots/spin-postrollback.png

**Step 2** Verify using task list
 
   * Click on ``TASKS`` and expand the rooback task, you can see that Spinnaker does many subtasks underthe hood

      *  .. figure:: /screenshots/spin-rollbacktask.png

**Step 2** Verify the application itself

   * Connect to <lab ip>:30020, the application shoul display the original color

      *  .. figure:: /screenshots/spin-appverify.png

.. _scalling:

Scalling Applications
~~~~~~~~~~~~~~~~~~~~~~

**Step 1** Verify current number of instances in command-line::

  $  kubectl get pods -n site

* There should be only 1 pod for site-production-v000 as seen below

  *  .. figure:: /screenshots/spin-pod.png

**Step 2** Scale up to three instances for v000
              
  * Return to Spinnaker Dashboard
  * Click ``Clusters``
  * Click ``V000`` server group
  * Click ``Server Group Actions``
  * Click ``Resize``
  * Set desired to 3, with reason as higher load, as seen below

    *   .. figure:: /screenshots/spin-resize.png
  * Type local and click ``Submit``

**Step 3** Verify that there are now three instances

  * Dashboard should show 3 green boxes

    * .. figure:: /screenshots/spin-deck3pods.png

.. code-block:: none

  $  kubectl get pods -n site

* There should be now be 3 pods for site-production-v000 as seen below

  *  .. figure:: /screenshots/spin-3pods.png
  
.. _terminate:

Terminating Instances
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Spinnker allow you to terminate instances using Deck, let's see what happens.

**Step 1** Watch the instances in command-line::

  $ watch kubectl get pods -n site
  
  * There should 3 pods for site-production-v000 as seen below

  *  .. figure:: /screenshots/spin-pod.png

**Step 2** Delete an instance from v000

  * Return to Spinnaker Dashboard
  * Click ``Clusters``
  * Click ``V000`` server group
  * Click on the right most instance (green rectangle)
  * Click ``Instance Actions``
  * Click ``Terminate``
  * Confirm on Screen below

    *   .. figure:: /screenshots/spin-terminate.png
  * Type local and click ``Submit``

**Step 3** What happened?

  * How come there are still 3 instances?
  * If you pay close attention to the pods you will notice one of the pods is a lot newer than the other 2
  * Because stack is Auto healing. the pod was replicated since we set desired pods to 3.

.. _availability:

Test Application Availability
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Since terminating instances does not allow us to confirm HA is working, we can test a different way.

**Step 1** Dergister two instance from v000

  * Return to Spinnaker Dashboard
  * Click ``Clusters``
  * Click ``V000`` server group
  * Click on the right most instance (green rectangle)
  * Click ``Instance Actions``
  * Click ``Deregister from Load Balancer``
  * Confirm on Screen below

    *   .. figure:: /screenshots/spin-deregister.png
  * Type local and click ``Submit``

**Step 2** Repeat Step 1 for the middle instance

**Step 3** Verify Production is still accessible

  * Open a new incognito window browser to: http://<lab ip>/30020

**Step 4** Repeat Step 1 for the remaining instance

**Step 5** Verify Production is NOT accessible

  * Open a new incognito window browser to: http://<lab ip>/30020
  
**Step 6** Reconnect all instance to load balancer at once

  * Return to Spinnaker Dashboard
  * Click ``Clusters``
  * Click ``V000`` server group
  * Click ``Server Group Actions``
  * Click ``Enable``

**Step 7** Verify Production is  accessible again

  * Open a new incognito window browser to: http://<lab ip>/30020

.. _restore:

Restore latest version of application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Step 1** Connect to Spinnaker cluster page

    * Point your browser to <labIP>:31000
    * Click ``Applications``
    * Click ``site``
    * Click ``Clusters``

**Step 2** Bring up Server Groups actions

    * Return to Spinnaker Dashboard
    * Click ``Clusters``
    * Click ``V000`` server group
    * Click ``Server Group Actions``
    * Click ``Rollback``
    * Restore to <Newest Server Group> and fill in reason as seen in picture below.

      *  .. figure:: /screenshots/spin-rollbackscreen.png
    * Type Local and click ``submit``

.. _destroy:

Destory old version of application to free up resources
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Step 1** Destroy server group v000

  * Return to Spinnaker Dashboard
  * Click ``Clusters``
  * Click ``V000`` server group
  * Click ``Server Group Actions``
  * Click ``Destroy``
  * Confirm on Screen below

    *   .. figure:: /screenshots/spin-destroy.png
  * Type local and click ``Submit``


.. admonition:: Checkpoint
    :class: conventions checkpoints

    * Roll back to privious version
    * Terminate Instances
    * Test HA
    * Destroy Server Groups

