.. include:: text_roles.txt

.. _cd_assignment:

*************
CD Assignment
*************
In this lab, we'll leverage what we've learned in previous chapters and create a workflow using pipelines.
Unlike previous chapters, you will not be given an exact prescription on how to complete the lab.
We encourage you to play arround, try different things, and make mistakes, this will help the learning process. 
Once you are done, contact the instructor to see the solution. You may reset the environment at anytime by running the clean
up step in this chapter.

.. sectnum::
    :suffix: .
    :start: 6

+----------------------------+----------------------------------------------------------------------+
| **Chapter Details**                                                                               |
+============================+======================================================================+
| **Chapter Goal**           | Using what we've learned, create a workflow to deploy an application |
|                            | with provided paramenters                                            |
+----------------------------+----------------------------------------------------------------------+
|                            | | :ref:`cleanup`                                                     |
|                            | | :ref:`create_workflow`                                             |
+----------------------------+----------------------------------------------------------------------+

.. raw:: pdf

    PageBreak

.. _cleanup:

Clean up site application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Clean up the site application and free up the resources

**Step 1** Destroy all server groups, load balancers::

  $ kubectl delete  ns site
    namespace "site" deleted

*  Spinnaker UI > application > Site > Config > Delete application

.. _create_workflow:

Create a Spinnaker application workflow using open ended instructions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Follow the guidelines below to create your own application workflow to 
demonstrate what you've learned and figure out some things you didn't.
When you are done, compare with the solution provided by the instructor.

#. Create a spinnaker application  and namespace named site2.
#. All artifacts used in this application should be in site2 namespace
#. site2 should have 4 load balancers with the following nodeports:
   
   * dev - 30015
   * test - 30016
   * prod - 30017
   * canary - 30018
#. Create 3 clusters, Each cluster should have 1 server group and be linked to the corresponding load balancers
   
   * dev - single instance
   * test - 2 instances
   * prod - 3 instances
#. Create a workflow consisting of 3 pipelines
   
   * **Deploy to dev** -This pipeline should consist of the following: triggered by code check in, deploy to dev environment using highlander strategy, automated qa, manual check by another developer
   * **Deploy to test** - This pipeline should consist of the following: triggered by successful deployment to Dev, check preconditions that at cluster size is greater or equal to 1, deploy to qa with red/black strategy,  automated qa, manual check, successful manual check should auto destroy older server group/version
   * **Promote to prod** - This pipeline should consist of the following: triggered by successful deployment to Test, check preconditions that at cluster size is greater or equal to 1, deploy a single instance of latest version to production and connect it to the canary load balancer, validate this version,   automated qa, manual check, successful manual check  of canary will deploy the new version to Production and replace existing 3 instances.use red/black strategy.
#. Trigger the workflow
   
   * edit the site.html and change the title to "MIRANTIS 2.0". tag it v2.0 and push the update to git.
   * Verify that the pipelines execute the way **YOU** intended them to.

.. admonition:: Checkpoint
    :class: conventions checkpoints

    * Leverage what you've learned and discover
