#!/usr/bin/env bash
# The script supports following formats:
#   .rst - reStructuredText format for workbooks
#   .html - layout HTML files
#   .py - configuration python file of Sphinx
# The script automatically detects type of the file and makes corresponding changes inline

# determining type of the file
if [ $1 ]
then
    echo "Processing ... $1"

    case $1 in
    *.html)
        # removing all content between {# SPC_REMOVE_BLOCK_START #} and {# SPC_REMOVE_BLOCK_END #}
        # adding content between {# SPC_ADD_BLOCK_START and SPC_ADD_BLOCK_END #} by removing multiline comment
        sed -i.bak -r -e '/\{# SPC_REMOVE_BLOCK_START #\}/,/\{# SPC_REMOVE_BLOCK_END #\}/d' -e '
        /\{# SPC_ADD_BLOCK_START|SPC_ADD_BLOCK_END #\}/d' $1
        ;;
    *.rst)
        # removing all content between .. SPC_REMOVE_BLOCK_START and .. SPC_REMOVE_BLOCK_END
        # adding content between .. SPC_ADD_BLOCK_START and .. SPC_ADD_BLOCK_end by unindenting lines
        sed -i.bak -r -e '/.. SPC_REMOVE_BLOCK_START/,/.. SPC_REMOVE_BLOCK_END/d' -e '
        /.. SPC_ADD_BLOCK_START/,/.. SPC_ADD_BLOCK_END/s/^  //' $1
        ;;
    *.py)
        # remove all content between # SPC_REMOVE_BLOCK_START and # SPC_REMOVE_BLOCK_END
        # sed -i .bak '/# SPC_REMOVE_BLOCK_START/,/# SPC_REMOVE_BLOCK_END/d' $1
        # adding content between # SPC_ADD_BLOCK_START and # SPC_ADD_BLOCK_END by uncommenting all lines
        sed -i.bak -r -e '/# SPC_REMOVE_BLOCK_START/,/# SPC_REMOVE_BLOCK_END/d' -e '
            /# SPC_ADD_BLOCK_START/,/# SPC_ADD_BLOCK_END/ {
            /# SPC_ADD_BLOCK_START/n
            /# SPC_ADD_BLOCK_END/ !{
                s/# //
                }
            }
            ' $1
        ;;
    *)
        echo "Format is not supported"
        ;;
    esac
else
    echo -e "Please use this syntax:\n  ./generate_spc_version.sh <file_name>\n\nThe following formats are supported: *.rst, *.py, *.html"
fi