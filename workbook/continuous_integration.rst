.. include:: text_roles.txt

.. _Continuous Integration:

**********************
Continuous Integration
**********************

.. sectnum::
    :suffix: .
    :start: 2

This chapter describes how to set up the CI using Git and Docker integration.

+----------------------------+----------------------------------------------------------------------+
| **Chapter Details**                                                                               |
+============================+======================================================================+
| **Chapter Goal**           | Setup the CI tooling with github and dockerhub in preparation for    |
|                            | Spinnaker Pipeline. Complete initial commit of application and       |
|                            | initial docker image.                                                |
+----------------------------+----------------------------------------------------------------------+
| **Chapter Sections**       | | :ref:`create_github_account`                                       |
|                            | | :ref:`create_github_repo`                                          |
|                            | | :ref:`create_dockerhub_account`                                    |
|                            | | :ref:`docker_repo`                                                 |
|                            | | :ref:`create_site`                                                 |
+----------------------------+----------------------------------------------------------------------+

.. raw:: pdf

    PageBreak

.. _create_github_account:

Set up Github Account
~~~~~~~~~~~~~~~~~~~~~
Each lab participant will need their own Github account. If you already have one, you can skip to Create Application Repository.

**Step 1** Go to |location_link1|.

.. |location_link1| raw:: html

   <a href="https://github.com/" target="_blank">https://github.com/</a>

**Step 2** Enter the following paramenters:

* Enter a Github ID.
* Enter your email address.
* Enter a password. For simplicity of this lab, you may use same password from dockerhub account.

**Step 3** Choose Sign up for GitHub, and then follow the instructions

.. _create_github_repo:

Create application repository (browser)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In this section, students will create the ``site`` application repository in Github

**Step 1** Create ``site`` repo

* Log in to GitHub: |location_link1|
* In Your repositories, choose New repository.
* On the Create a new repository page, do the following

  * In the Repository name box, type ``site``.
  * Select ``Public``
  * Clear the Initialize this repository with a README check box
  * Click ``Create Repository``

.. _create_dockerhub_account:

Create DockerHub account (Browser)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Each lab participant will need their own DockerHub account. If you already have one, you can skip to adding your docker repository.
Your Docker ID becomes your user namespace for hosted Docker services, and becomes your username on the Docker Forums

**Step 1** Go to |location_link|.

.. |location_link| raw:: html

   <a href="https://hub.docker.com/" target="_blank">https://hub.docker.com</a>

**Step 2** Enter the following paramenters and click ``Sign Up``:

* Enter a Docker ID.
* Enter your email address.
* Enter a password.

**Step 3** Check your email and verify your address.

.. |notes| image:: images/notes.gif

.. admonition:: Note
    :class: conventions notes

    You cannot log in with your Docker ID until you verify your email address.

**Step 4** Record your Docker ID and your Docker password. They will be used when the workbook references <dockerid> <dockerpass>

.. _docker_repo:

Set up Docker automatic builds for *site* repo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This steps configures docker to trigger automatic build when source code is committed to Github.

**Step 1** Login  |location_link|.

**Step 2** Create Automated Build Repo

* Click the ``Create`` drop down
* Choose ``Create Autmated Build``
* Click ``Link Accounts``
* Click ``Link Github``
* Select ``Public and Private``
* Sign in to Github (if required)
* Click ``Authorize Docker``
* Click the ``Create`` drop down
* Choose ``Create Autmated Build``
* Choose ``Github``
* Choose ``site`` repo
* Short Decription: Spinnaker lab application.
* Leave everything else default Click ``Create``

**Step 3** Configure build to trigger with new versions

* Click ``Build Settings``
* Choose `` Tag`` from drop down
* Clear ``Name`` field by removing ``master``
* Leave ``Docker file location`` as is
* Clear  ``Docker tag name`` by removing ``latest``
* Click ``Save Changes``


.. _create_site:

Creating the initial build v1.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This step uploads the application Github and Triggers the initial image build in Dockerhub

**Step 1** Login to the lab provided for you::

    $ ssh smashware@<lab IP>
    password: dev0p$101

**Step 2** Clone the site repo::

  $ git clone https://smashwareio@bitbucket.org/smashware/site.git
  $ cd site

**Step 3** Rewrite origin to your own repo::

  $ git remote remove origin
  $ git remote add origin https://github.com/<githubid>/site.git

**Step 4** Push the site data::

  $ git tag v1.0
  $ git push --set-upstream origin master --tags
  Username for 'https://github.com': your_github_account
  Password for 'https://github.com': your_github_password

**Step 5** Verify docker image was built in Dockerhub:

  * Log in to Docker hub and check Build Details
  * Veirfy that Build 1.0 was create as seen below
  
    *  .. figure:: /screenshots/spin-dockerhub.png

.. admonition:: Checkpoint
    :class: conventions checkpoints

    * Create github and dockerhub accounts
    * Add you application repo to Github
    * Link Dockerhub to Github so new images are triggered with each code commit
    * Docker hub auto build image when new code is pushed to git
   
