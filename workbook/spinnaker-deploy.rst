.. include:: text_roles.txt

.. _spinnaker_deployment:

********************
Spinnaker Deployment
********************

.. sectnum::
    :suffix: .
    :start: 3

This chapter describes how to install Spinnaker using helm on a K8s cluster.

+----------------------------+----------------------------------------------------------------------+
| **Chapter Details**                                                                               |
+============================+======================================================================+
| **Chapter Goal**           | Install Spinnaker using Helm chart. We will install helm first on the|
|                            | K8s Cluster and then use it to deploy Spinnaker. Deploy Jenkins.     |
|                            | Enable and Access Spinnaker and Jenkins dashboard.                   |
+----------------------------+----------------------------------------------------------------------+
| **Chapter Sections**       | | :ref:`install_kube_cluster`                                        |
|                            | | :ref:`install_helm`                                                |
|                            | | :ref:`install_spinnaker`                                           |
|                            | | :ref:`expose_spinnaker_dashboard`                                  |
|                            | | :ref:`expose_jenkins_dashboard`                                    |
|                            | | :ref:`configure_jenkins_job`                                       |
+----------------------------+----------------------------------------------------------------------+

.. raw:: pdf

    PageBreak

.. _install_kube_cluster:

Deploying Kubernetes Cluster
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Before we install Spinnaker, we need to get out Kubenestes cluster set up.

.. |important| image:: images/important.bmp

.. admonition:: Important
    :class: conventions important

    If you are contnuing this lab from another course like kube100, you should clean up the environment by running::

      $ sudo ~/devops101/bin/cleanup.sh

**Step 1** Login to the lab provided for you::

    $ ssh smashware@<lab IP>
    password: dev0p$101

**Step 2** Install Docker and Kubernetes::

    $ cd ~
    $ git clone https://smashwareio@bitbucket.org/smashware/devops101.git
    $ sudo ~/devops101/bin/install-docker.sh
    $ sudo ~/devops101/bin/install-k8s.sh
    $ kubectl version
    $ minikube version

.. _install_helm:

Install Helm and Tiller
~~~~~~~~~~~~~~~~~~~~~~~
Helm is a package manager for Kubernetes that helps you manage Kubernetes applications. Helm Charts helps you define, install,
and upgrade even the most complex Kubernetes application.
Helm is comprised of client and server components. The client is refered to as Helm and the server part is called Tiller.

**Step 1** Get the stable version of helm that has been verified for this lab ::

    $ wget https://kubernetes-helm.storage.googleapis.com/helm-v2.7.2-linux-amd64.tar.gz

**Step 2** Unpack and install helm client::

    $ tar -zxvf helm-v2.7.2-linux-amd64.tar.gz
    $ sudo mv ~/linux-amd64/helm /usr/local/bin/

**Step 3** Install helm server (Tiller)::

    $ helm init

**Step 4** Verify Helm & Tiller installation::

    $ helm version
    Client: &version.Version{SemVer:"v2.7.2", GitCommit:"a80231648a1473929271764b920a8e346f6de844", GitTreeState:"clean"}
    Server: &version.Version{SemVer:"v2.7.2", GitCommit:"a80231648a1473929271764b920a8e346f6de844", GitTreeState:"clean"}

.. _install_spinnaker:

Install Spinnaker
~~~~~~~~~~~~~~~~~
There are a few ways of installing Spinnaker, this one below uses a helm chart. value.yaml is a file that helm reads to set up spinnkaer how you want it.
Spinniker services will be installed in to containers in name space ``spinnaker``.

**Step 1** Update values.yaml so that you spinnaker can load images from your Docker Hub::

    $ cd ~/devops101/spinnaker/
    $ vim values.yaml
    Fill in your <dockerid> on line 15 and leave everything else untouched.

**Step 3** Install spinnaker dependencies with helm::
    
    $ helm dep build

**Step 2** Install spinnaker with helm::

    $ helm install -n demo --namespace spinnaker -f values.yaml .
    This will take 3 minutes.

**Step 3** Verify the spinnaker pods state::

    $ helm list
    NAME    REVISION        UPDATED                         STATUS          CHART           NAMESPACE
    demo    1               Mon Mar 19 06:32:21 2018        DEPLOYED        spinnaker-0.4.0 spinnaker
    $ kubectl -n spinnaker get pods
    NAME                                          READY     STATUS    RESTARTS   AGE
    demo-jenkins-696f94647c-tlwng                 1/1       Running   0          8m
    demo-minio-6c68d75d8b-v4ctb                   1/1       Running   0          8m
    demo-redis-7f876c8f69-zrbxk                   1/1       Running   0          8m
    demo-spinnaker-clouddriver-7f8c8944fd-rh9rw   1/1       Running   0          8m
    demo-spinnaker-deck-5f857f4f99-mbrd6          1/1       Running   0          8m
    demo-spinnaker-echo-57466cf475-schds          1/1       Running   0          8m
    demo-spinnaker-front50-54886756d9-7b9tv       1/1       Running   0          8m
    demo-spinnaker-gate-86589fbdc4-h427p          1/1       Running   0          8m
    demo-spinnaker-igor-c67dc5647-2zlln           1/1       Running   0          8m
    demo-spinnaker-orca-596fdf99b7-l697n          1/1       Running   0          8m
    demo-spinnaker-rosco-65fdf5cfdf-82k6w         1/1       Running   0          8m


.. _expose_spinnaker_dashboard:

Expose Spinnaker Dashboard
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Step 1** Expose the Spinnaker Dashboard::

    $ kubectl -n spinnaker expose svc demo-spinnaker-deck --name expose-deck --type=NodePort
    service "expose-deck" exposed

**Step 2** Update Spinnkaer UI nodeport to 31000::

    $ kubectl -n spinnaker edit svc expose-deck
    change nodePort to 31000

**Step 3** Get the Spinnaker Dashboard url ::

    $ echo `curl ifconfig.co`:31000

**Step 4** Open the Spinnaker Dashboard (Browser)


.. _expose_jenkins_dashboard:

Expose Jenkins Dashboard
~~~~~~~~~~~~~~~~~~~~~~~~

**Step 1** Expose the jenkins dashboard::

    $ kubectl -n spinnaker expose svc demo-jenkins --name expose-jenkins --type=NodePort
      service "expose-jenkins" exposed

**Step 2** Update Jenkins UI nodeport to 31001::

    $ kubectl -n spinnaker edit svc expose-jenkins
    change nodePort to 31001

**Step 2** Get the Jenkins Dahsboard url::

    $ echo `curl ifconfig.co`:31001

**Step 3** Access the Jenkin URL and verify you can connect (Browser)

.. _configure_jenkins_job:

Configure Jenkins
~~~~~~~~~~~~~~~~~
Set up Jenkins QA job for automated testing in pipelines.

**Step 1** Upload the jenkins job that triggers a new dockerhub image after a code commit::

    $  sudo ~/devops101/bin/upload-jenkins-job.sh

**Step 2** Get the jenkins url::

    $ echo `curl -s ifconfig.co`:31001

**Step 3** Reload QA site (Browser)

    * Click ``Manage Jenkins``
    * Click  ``Reload Configuration from Disk``
    * QA site should be added to the home page as seen below
    
      * .. figure:: /screenshots/spin-qasite.png

.. admonition:: Checkpoint
    :class: conventions checkpoints

    * Deploy Foundation ( Docker -> Kubernetes )
    * Install Helm and Tiller
    * Configure value.yaml
    * Deploy Spinnaker
    * Access Spinnaker UI and Jenkins UI
   
