.. include:: text_roles.txt

*******
Preface
*******

**Welcome**

Welcome to *SPIN50: Spinnaker Fundamentals*. These labs cover the
critical skills needed to instal, configure, and operate  Spinnaker for Continuous Delivery.

**Lab Objectives**

After completing these labs, you will understand how to:

* Deploy Spinnaker
* Integrate Spinnaker with CI tools 
* Create  Spinnaker pipelines
* Operate Spinnaker

\ \

**This Guide Structure**

This guide is designed to complement instructor-led presentations by providing
step-by-step instructions for hands-on exercises.

Every chapter of this guide starts with *Chapter Details* and finishes with
*Checkpoint*.

.. admonition:: Chapter Details

    Each chapter opens with a short description of the chapter's goals and
    objectives, and an outline of the included sections.

.. |checkpoints| image:: images/checkpoint.png

.. admonition:: Checkpoint
    :class: conventions checkpoints

    Each chapter ends with a summary of what was covered in the section. You
    can use *Checkpoints* to verify your understanding of the topic.

There are a number of notations that are used throughout the guide. They are
here to provide you with extra information on the task at hand. Do not execute
the steps listed in the notations.

.. |notes| image:: images/notes.gif

.. admonition:: Notes
    :class: conventions notes

    Notes are tips, shortcuts or alternative approaches for the task at hand.

.. |references| image:: images/references.bmp

.. admonition:: Reference
    :class: conventions references

    References are links to external documentation relevant to a subject.

.. |important| image:: images/important.bmp

.. admonition:: Important
    :class: conventions important

    Important boxes indicate a warning or caution. They detail things that are
    easily missed and should not be overlooked. This is information that you
    must be aware of before proceeding.

Code blocks:

.. code-block:: none

    Commands to be executed in a terminal window
    and an example of the output

Commands need to be executed exactly as they are written with the exception of
the command prompt and the parts enclosed in ``< >``, which need to be
substituted with your data (typically either the lab environment IP address or
ID of a previously created entity).

Example of the code block:

.. code-block:: none

    smashware@lab:~$ ping -c 5 <ip-address>
    PING <ip-address> (<ip-address>) 56(84) bytes of data.
    64 bytes from 192.168.224.2: icmp_seq=1 ttl=64 time=0.053 ms
    64 bytes from 192.168.224.2: icmp_seq=2 ttl=64 time=0.052 ms
    ...

    --- 192.168.224.2 ping statistics ---
    5 packets transmitted, 5 received, 0% packet loss, time 3996ms
    rtt min/avg/max/mdev = 0.045/0.051/0.053/0.005 ms

Here ``smashware@lab:~$`` is the command prompt and you are expected to execute the
command ``ping -c 5 <ip-address>`` where you should substitute ``<ip-address>``
by the actual IP address given in the context of that step. The actual output
may be different from the ones shown in the examples. For longer outputs, the
non-important part is excluded and is typically substituted with "``...``".


.. only:: html

    **Contents**

.. toctree::
    :maxdepth: 1

    environment
    continuous_integration
    spinnaker-deploy
    cd-pipeline
    application-management
    cd-assignment
